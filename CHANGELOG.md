<!--
Guiding Principles:

Changelogs are for humans, not machines.
There should be an entry for every single version.
The same types of changes should be grouped.
Versions and sections should be linkable.
The latest version comes first.
The release date of each version is displayed.
Mention whether you follow Semantic Versioning.

Usage:

Change log entries are to be added to the Unreleased section under the
appropriate stanza (see below). Each entry should ideally include a tag and
the Github issue reference in the following format:

* (<tag>) \#<issue-number> message

The issue numbers will later be link-ified during the release process so you do
not have to worry about including a link manually, but you can if you wish.

Types of changes (Stanzas):

"Features" for new features.
"Improvements" for changes in existing functionality.
"Deprecated" for soon-to-be removed features.
"Bug Fixes" for any bug fixes.
"API Breaking" for breaking exported APIs used by developers building on SDK.
Ref: https://keepachangelog.com/en/1.0.0/
-->

# Changelog

## [Unreleased]

### Features

### Improvements

### API Breaking Changes

### Bug Fixes

### Deprecated

## v1.2.6_qc - 2021-12-03

### Features

* (API) SendContractManageRequestWithTxId 发送合约管理请求时可指定txid
* (配置) 配置文件中可配置retry策略。
    * retry_limit: 10 # 同步交易结果模式下，轮训获取交易结果时的最大轮训次数，删除此项或设为<=0则使用默认值 10
    * retry_interval: 500 # 同步交易结果模式下，每次轮训交易结果时的等待时间，单位：ms 删除此项或设为<=0则使用默认值 500
* (Grpc client) grpc客户端发送消息时，可设置允许单条message大小的最大值(MB)

### Improvements

* (订阅) 支持订阅断线自动重连机制
* (订阅) 支持合约事件按照区块高度订阅历史事件

### Bug Fixes

* (evm) 修复evm创建合约时，解析evm合约bin文件编码的问题
* (国密) tjgm升级，修复旧版本在公钥长度不为32会产生错误的问题
